#include <queue>
#include <chrono>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <unordered_map>

size_t num_steps = 2;
size_t* steps;
unsigned short starting_ease = 250;
unsigned short easy_bonus = 130;
unsigned short new_cards_per_day = 20;
unsigned short max_reviews_per_day = 9999;
unsigned short graduating_interval = 1;
unsigned short easy_interval = 4;
unsigned short new_interval = 0;
unsigned short minimum_interval = 1;

struct parser
{
  parser(std::string path)
  {
    std::ifstream file(path);
    std::ostringstream data;
    std::copy(std::istreambuf_iterator<char>(file),
	      std::istreambuf_iterator<char>(),
	      std::ostreambuf_iterator<char>(data));
    text = data.str();
    start = first = text.data();
    last = first + text.size();
  }

  void parse_conf_file()
  {
    // pass 1: remove spaces and comments
    std::string no_space_text;
    while(!eof())
    {
      if(!std::isspace(*first))
	no_space_text += *first;
      ++first;
      if(*first == '#')
	while (!eof() && *first != '\n')
	  ++first;
    }

    text = no_space_text;
    start = first = text.data();
    last = first + text.size();

    // pass 2: parse configurable variables
    while(!eof())
    {
      std::string buf;
      if (std::isalpha(*first) || *first == '_')
      {
	do
	  buf += *(first++);
	while (std::isalpha(*first) || *first == '_');

	parse_word(buf);
      }
    }
  }

private:
  bool eof() const
  {
    return first == last;
  }

  void parse_word(std::string const& buf)
  {
    auto conf_var_map_iter = config_variables.find(buf);
    if (conf_var_map_iter != config_variables.end())
    {
      if (*first != '=')
      {
	std::stringstream ss;
	ss << "syntax error near assignment of variable "
	   << conf_var_map_iter->first;
	throw std::runtime_error(ss.str());
      }
      ++first;

      std::string value_buf;
      if (std::isdigit(*first))
      {
	do
	  value_buf += *first++;
	while (std::isdigit(*first));
      } else {
	std::stringstream ss;
	ss << "syntax error: invalid value passed to variable "
	   << conf_var_map_iter->first;
	throw std::runtime_error(ss.str());
      }

      int value = std::stoi(value_buf);
      if (value < 0)
      {
	std::stringstream ss;
	ss << "passing negative value to variable " << conf_var_map_iter->first;
	throw std::runtime_error(ss.str());
      }
      
      switch (conf_var_map_iter->second)
      {
      case NUM_STEPS:
	num_steps = value;
	break;
      case STEPS:
	break;
      case STARTING_EASE:
	starting_ease = value;
	break;
      case EASY_BONUS:
	easy_bonus = value;
	break;
      case NEW_CARDS_PER_DAY:
	new_cards_per_day = value;
	break;
      case MAX_REVIEWS_PER_DAY:
	max_reviews_per_day = value;
	break;
      case GRADUATING_INTERVAL:
	graduating_interval = value;
	break;
      case EASY_INTERVAL:
	easy_interval = value;
	break;
      case NEW_INTERVAL:
	new_interval = value;
	break;
      case MINIMUM_INTERVAL:
	minimum_interval = value;
	break;
      }
    } else {
      std::stringstream ss;
      ss << "unknown identifier " << buf;
      throw std::runtime_error(ss.str());
    }
  }

  enum conf
  {
    NUM_STEPS,
    STEPS,
    STARTING_EASE,
    EASY_BONUS,
    NEW_CARDS_PER_DAY,
    MAX_REVIEWS_PER_DAY,
    GRADUATING_INTERVAL,
    EASY_INTERVAL,
    NEW_INTERVAL,
    MINIMUM_INTERVAL,
  };

  const std::unordered_map<std::string, conf> config_variables =
  {
    {"num_steps", NUM_STEPS},
    {"steps", STEPS},
    {"starting_ease", STARTING_EASE},
    {"easy_bonus", EASY_BONUS},
    {"new_cards_per_day", NEW_CARDS_PER_DAY},
    {"max_reviews_per_day", MAX_REVIEWS_PER_DAY},
    {"graduating_interval", GRADUATING_INTERVAL},
    {"easy_interval", EASY_INTERVAL},
    {"new_interval", NEW_INTERVAL},
    {"minimum_interval", MINIMUM_INTERVAL},
  };

  const char* first = nullptr, *last = nullptr, *start = nullptr;
  std::string text;
};

enum card_kind
{
  ck_review,
  ck_learn,
  ck_relearn,
  ck_new,
};

enum answer_kind
{
  ak_again,
  ak_hard,
  ak_good,
  ak_easy,
};

const char* answer_kind_name(card_kind k)
{
  switch(k)
  {
  case ck_review:
    return "review";
  case ck_learn:
    return "learn";
  case ck_relearn:
    return "relearn";
  case ck_new:
    return "new";
  }

  return ""; // silence warning
}

struct card
{
  const char* data;
  card_kind kind = ck_new;
  size_t reps = 0;
  size_t step = 0; 
  size_t interval = 0;

  void adjust_ease(int value)
  {
    ease += value;
    if (ease < 130)
      ease = 130;
  }

  size_t get_ease() const
  {
    return ease;
  }


  card()
  {}

  card(const char* data, card_kind k)
    : data(data), kind(k)
  {}

private:
  size_t ease = starting_ease;
};

void respond(card& c, answer_kind answer)
{
  if (c.kind == ck_new)
    c.kind = ck_learn;

  if (c.kind == ck_learn || c.kind == ck_relearn)
  {
    switch (answer)
    {
    case ak_again:
      c.step = 0;
      break;

    case ak_good:
      if (c.step < (num_steps - 1))
	++c.step;
      else {
	c.kind = ck_review;
	c.interval = graduating_interval;
      }
      break;

    case ak_easy:
      c.kind = ck_review;
      c.interval = easy_interval;
      break;

    default: break; // silence warning
    }
  } else if (c.kind == ck_review) {
    size_t start_interval = c.interval;

    switch (answer)
    {
    case ak_again:
      c.kind = ck_relearn;
      c.interval = 0;
      c.step = 0;
      c.adjust_ease(-20);
      c.interval *= std::round(new_interval / 100.f);
      break;

    case ak_hard:
      c.adjust_ease(-15);
      c.interval = std::round(c.interval * 1.2);
      break;

    case ak_good:
      c.interval = std::round(c.interval * (c.get_ease() / 100.f));
      break;

    case ak_easy:
      c.interval =
	std::round(c.interval * ((c.get_ease() / 100.f) + (easy_bonus / 100.f)));
      c.adjust_ease(15);
      break;
    }

    if (c.interval < minimum_interval && c.kind != ck_relearn)
      c.interval = minimum_interval;

    // force the interval to be one day longer than its previous interval.
    if (c.interval == start_interval)
      ++c.interval;
  }

  ++c.reps;
}

int main()
{
  parser p(".manarc");
  p.parse_conf_file();
  std::cout << "PARSED FILE\n";
  std::cout << "num_steps: " << num_steps << '\n';

  steps = new size_t[num_steps];
  steps[0] = 1;
  steps[1] = 1;
  // steps[2] = 10;

  card c1("1", ck_new), c2("2", ck_new), c3("3", ck_new);

  std::queue<card> new_queue;
  std::queue<card> review_queue;
  std::queue<card> learn_queue;

  new_queue.push(c1);
  // new_queue.push(c2);
  // new_queue.push(c3);

  std::cout << "begin reviewing\n";

  unsigned new_cards = 0;
  while(true)
  {
    if (new_queue.empty() && learn_queue.empty() && review_queue.empty())
      break;

    card current;

    if (new_cards < (new_cards_per_day - 1) && !new_queue.empty())
    {
      current = new_queue.front();
      new_queue.pop();
      ++new_cards;
    } else if (!learn_queue.empty())
    {
      current = learn_queue.front();
      learn_queue.pop();
    } else if (!review_queue.empty())
    {
      current = review_queue.front();
      review_queue.pop();
    }
    
    std::cout << "CARD: " << current.data << '\n';
    std::cout << "INTERVAL: " << current.interval << '\n';
    std::cout << "EASE: " << current.get_ease() << '\n';
    std::cout << "STEP: " << current.step << '\n';
    std::cout << "KIND: " << answer_kind_name(current.kind) << '\n';
    std::cout << "answer 0: again, 1: hard, 2: good, 3: easy\n";
    std::string ans;
    std::cin >> ans;
    respond(current, (answer_kind)std::stoi(ans));

    if (current.kind == ck_learn || current.kind == ck_relearn)
      learn_queue.push(current);
    else if (current.kind == ck_review)
      review_queue.push(current);
  }

  delete[] steps;
}
